import axios from 'axios';

const api = axios.create({
    baseURL: 'https://alvoapp-6cc18.firebaseio.com',
})

export default api;