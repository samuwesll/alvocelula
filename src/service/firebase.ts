import * as firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCEfhGbun2lGiAWPROLbBpw9Ssbdc_GKZc",
    authDomain: "alvoapp-6cc18.firebaseapp.com",
    databaseURL: "https://alvoapp-6cc18.firebaseio.com",
    projectId: "alvoapp-6cc18",
    storageBucket: "alvoapp-6cc18.appspot.com",
    messagingSenderId: "39519071157",
    appId: "1:39519071157:web:3faa973e34345017be07fc"
};

firebase.initializeApp(firebaseConfig);

const dbh = firebase.firestore();

export default firebase;