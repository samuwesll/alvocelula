interface EventoMensalModel {
    id?: string,
    diaSemana?: number,
    diaRefInicio?: number,
    diaRefFinal?: number,
    nome?: string,
    local?: string,
    observacao?: string,
}