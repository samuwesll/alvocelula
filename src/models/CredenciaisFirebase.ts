interface CredenciaisFirebase {
    displayName: string,
    createdAt?: string,
    email?: string
    lastLoginAt?: string,
    stsTokenManager?: {
        accessToken: string,
        apiKey: string,
        expirationTime: string,
        refreshToken: string
    },
    uid?: string,
}