interface LiderModel {
    id?: string,
    arrayCelula?: string[],
    perfil?: string,
    liderId?: string,
    credenciais?: CredenciaisFirebase,
}