interface CelulaModel {
    id?: string,
    nome?: string,
    dia?: number,
    horario?: string,
    dataInicio?: string,
    idLideres?: string[],
    perfil?: string,
}