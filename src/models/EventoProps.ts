interface EventoProps {
    nome?: '',
    diaSemana?: '',
    horario?: '',
    celula?: boolean, 
    relatorio?: boolean,
    dia?: string,
    eventoSemanal?: EventoSemanalModel,
}

export default EventoProps;