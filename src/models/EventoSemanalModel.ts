interface EventoSemanalModel {
    id?: string,
    diaSemana?: number,
    evento?: string,
    nome?: string,
    local?: string,
    horario?: string,
}