interface MembroModel {
    id?: string,
    nome?: string,
    apelido?: string,
    dataNascimento?: string,
    telefone?: string,
    dataCadastrado?: string,
    dataAlteracao?: string,
}