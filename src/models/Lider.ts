interface Lider {
    id?: string,
    nomeCompleto?: string,
    dataNascimento?: string,
    corRede?: string,
}