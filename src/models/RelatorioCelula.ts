interface RelatorioCelula {
    id?: string,
    data?: string,
    idLider?: string,
    visitantes?: number,
    quiloAmor?: number,
    idMembrosPresentes?: string[],
    dataEnvio?: string,
    valorOferta?: number,
}