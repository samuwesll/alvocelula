import { StyleSheet } from "react-native";
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
    body: {
        alignItems: 'center',
    },
    bodyCard: {
        // flexDirection: 'row',
        // alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 30,
        justifyContent: 'space-between',
        width: '90%'
    },
    bodyCardHeader: {
        flexDirection: 'row'
    },
    bodyCardTitle: {
        marginLeft: 10,
        fontSize: 20,
        fontFamily: 'Caladea_400Regular_Italic',
    },
    bodyCardSession: {
        marginTop: 20,
    },
    bodyCardSessionText: {
        margin: 5,
    },
    bodyCardSessionTitle: {
        fontWeight: 'bold',
    },
    bodyCardSessionDataNasc: {
        marginTop: 4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    footer: {
        marginTop: 30,
    },
    footerButton: {
        backgroundColor: 'black',
        padding: 7,
        borderRadius: 8,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
    },
    footerButtonText: {
        color: '#ffff',
        marginLeft: 10,
        fontWeight: '700'
    },
    modalContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 40,
    },
    modalBody: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: '85%'
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 18
    },
    modalInfo: {
        alignItems: 'center',
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '90%'
    },
    modalButtonFechar: {
        marginTop: 20,
        backgroundColor: "red",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    },
    modalButtonOk: {
        marginTop: 20,
        backgroundColor: "green",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    }
})

export default styles;