import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, Modal, Alert, TouchableOpacity, AsyncStorage } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';
import firebase from '../../service/firebase';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import PageHeader from '../../components/PageHeader';

function Ajuste() {

    const [user, setLogin] = useState<LiderModel>();
    const [lider, setLider] = useState<Lider>({});
    const [celula, setCelula] = useState<CelulaModel>({});
    const [modalVisibleSair, setModalVisibleSair] = useState(false);

    const navigation = useNavigation();

    async function consultaStorage() {
        let userLider: LiderModel = {};
        let lid: Lider = {};
        let cell: CelulaModel = {};
        await AsyncStorage.getItem('login').then(response => {
            userLider = JSON.parse(response as any);
        });
        firebase.database().ref(`tb_lider/${userLider.liderId}`).on('value', response => {
            lid = { id: response.key, ...response.val() };
            setLider(lid)
        })
        firebase.database().ref(`tb_celula/${userLider.arrayCelula?.values().next().value}`).on('value', response => {
            cell = { id: response.key, ...response.val() }
        })
        setCelula(cell)
    }

    function dataAniversario(dataString: string): string {
        const dataNascimento = new Date(new Date(dataString).setDate(new Date(dataString).getDate() + 1));
        const monthNamesShort = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        const dia = dataNascimento.getDate().toString().padStart(2, '0');
        const mes = monthNamesShort[dataNascimento.getMonth()];
        return `${dia}-${mes}`
    }

    function calcularIdade(dataNasc: string): number {
        var dataAtual = new Date();
        var anoAtual = dataAtual.getFullYear();
        var diaNasc = new Date(dataNasc).getDate();
        var mesNasc = new Date(dataNasc).getMonth();
        var anoNasc = new Date(dataNasc).getFullYear();
        var idade = anoAtual - anoNasc;
        var mesAtual = dataAtual.getMonth() + 1;
        if (mesAtual < mesNasc) {
            idade--;
        } else {
            if (mesAtual == mesNasc) {
                if (new Date().getDate() < diaNasc) {
                    idade--;
                }
            }
        }
        return idade;
    }

    function diaSemana(numeroSemana: number): string {
        // let dayNamesShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];
        let dayNamesShort = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
        return dayNamesShort[numeroSemana]
    }

    function logoff() {
        AsyncStorage.clear().then(response => {
            navigation.navigate('Login')
        })
    }

    useEffect(() => {
        consultaStorage();
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <PageHeader title="Minhas Informações" />

            <ScrollView>
                <View style={styles.body}>

                    <View style={styles.bodyCard}>
                        <View style={styles.bodyCardHeader}>
                            <Feather name="user" size={24} color="black" />
                            <Text style={styles.bodyCardTitle}>Meu perfil</Text>
                        </View>

                        <View style={styles.bodyCardSession}>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Nome:</Text>
                                {lider?.nomeCompleto}
                            </Text>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Rede:</Text>
                                {lider?.corRede}
                            </Text>
                            <View style={styles.bodyCardSessionDataNasc}>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Data nasc.:</Text>
                                    {dataAniversario(lider.dataNascimento as string)}
                                </Text>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Idade:</Text>
                                    {calcularIdade(lider.dataNascimento as string)}
                                </Text>

                            </View>
                        </View>
                    </View>

                    <View style={styles.bodyCard}>
                        <View style={styles.bodyCardHeader}>
                            <MaterialCommunityIcons name="dna" size={24} color="black" />
                            <Text style={styles.bodyCardTitle}>Célula</Text>
                        </View>

                        <View style={styles.bodyCardSession}>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Nome:</Text>
                                {celula.nome}
                            </Text>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Perfil:</Text>
                                {celula.perfil}
                            </Text>

                            <View style={styles.bodyCardSessionDataNasc}>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Data inicio:</Text>
                                    {dataAniversario(celula.dataInicio as string)}
                                </Text>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Horario:</Text>
                                    {celula.horario}
                                </Text>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>dia Sem.:</Text>
                                    {diaSemana(celula.dia as number)}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.footerButton} onPress={() => { setModalVisibleSair(true) }}>
                            <AntDesign name="export2" size={26} color="#fff" />
                            <Text style={styles.footerButtonText}>Sair</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <Modal animationType='slide' transparent={true} visible={modalVisibleSair} onRequestClose={() => { Alert.alert("Modal has been closed."); }}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalBody}>
                        <Text style={styles.modalTitle}>LogOff</Text>
                        <View style={styles.modalInfo}>
                            <Text>Tem certeza que deseja sair da conta?</Text>
                        </View>

                        <View style={styles.modalFooter}>
                            <TouchableOpacity style={styles.modalButtonFechar} onPress={() => { setModalVisibleSair(!modalVisibleSair) }}>
                                <AntDesign style={{ marginRight: 10 }} name="dislike2" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    Não
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.modalButtonOk} onPress={() => { setModalVisibleSair(!modalVisibleSair), logoff() }}>
                                <AntDesign style={{ marginRight: 10 }} name="like2" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    Sim
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default Ajuste;