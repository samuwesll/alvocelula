import React, { useState, useEffect } from 'react';
import { View, Text, AsyncStorage, SafeAreaView, ScrollView, TouchableOpacity, Modal, Alert, Linking } from 'react-native';
import firebase from '../../service/firebase';
import { AntDesign, FontAwesome5 } from '@expo/vector-icons';
import DatePicker from 'react-native-datepicker'

import styles from './styles';
import PageHeader from '../../components/PageHeader';
import { TextInput } from 'react-native-gesture-handler';

const Membros = () => {

    const [celula, setCelula] = useState<CelulaModel>();
    const [membros, setMembros] = useState<MembroModel[]>([]);
    const [modalVisible, setModalVisible] = useState(false);

    const [membroSelecionado, setMembroSelecionado] = useState<MembroModel>({});

    const [textNome, setTextNome] = useState<string>()
    const [textApelido, setTextApelido] = useState<string>()
    const [textTelefone, setTextTelefone] = useState<string>()
    const [textDateString, setTextDateString] = useState<string>()

    function consultarStorage() {
        let login: LiderModel = {}
        AsyncStorage.getItem('login').then(result => {
            login = JSON.parse(result as any);
            consultaFirebase(login.arrayCelula?.values().next().value)
        });
    }

    function consultaFirebase(idCelula: string) {
        firebase.database().ref(`tb_membro/${idCelula}`).on('value', (resul => {
            const mem: MembroModel[] = [];
            resul.forEach(r => {
                mem.push({ id: r.key, ...r.val() })
            });
            setMembros(mem);
        }));
        firebase.database().ref(`tb_celula/${idCelula}`).on('value', response => {
            setCelula({id: response.key, ...response.val()})
        })
    }

    function dataAniversario(dataString: string): string {
        const dataNascimento = new Date(new Date(dataString).setDate(new Date(dataString).getDate() + 1));
        const monthNamesShort = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        const dia = dataNascimento.getDate().toString().padStart(2, '0');
        const mes = monthNamesShort[dataNascimento.getMonth()];
        return `${dia}-${mes}`
    }

    function editarMembro(memb: MembroModel) {
        setMembroSelecionado(memb)
        setTextNome(memb.nome)
        setTextApelido(memb.apelido)
        setTextTelefone(memb.telefone)
        setTextDateString(timeToString(memb.dataNascimento as string))
    }

    function timeToString(data: string): string {
        return new Date(data).toISOString()
    }

    function salvarMembro() {
        if (membroSelecionado.nome) {
            const updateMembro: MembroModel = {
                apelido: textApelido,
                nome: textNome,
                telefone: textTelefone,
                dataNascimento: timeToString(textDateString as string),
                dataAlteracao: new Date().toISOString(),
            };
            const validacao: ValidacaoCampo = verificarCamposVazios(updateMembro)
            if (validacao.erro) {
                setModalVisible(!modalVisible)
                Alert.alert('Erro ao cadastrar', validacao.mensagem);
            } else {
                firebase.database().ref(`tb_membro/${celula?.id}/${membroSelecionado.id}`).update(updateMembro).then(response => {
                    Alert.alert(`Informações atualizada`, `informações do ${updateMembro.apelido} realizada com sucesso`,)
                })
                firebase.database().ref(`tb_membro/${celula?.id}`).on('value', membrosResp => {
                    let mem: MembroModel[] = [];
                    membrosResp.forEach(elem => {
                        mem.push({ id: elem.key, ...elem.val() })
                    })
                    AsyncStorage.setItem('membros', JSON.stringify(mem))
                });
            }
        } else {
            if (textDateString == '') {
                setModalVisible(!modalVisible)
                return Alert.alert('Erro ao cadastrar', 'campo DATA-NASCIMENTO em branco');
            }
            const novoMembro: MembroModel = {
                apelido: textApelido,
                nome: textNome,
                telefone: textTelefone,
                dataNascimento: timeToString(textDateString as string),
                dataAlteracao: '',
                dataCadastrado: new Date().toISOString()
            };
            const validacao: ValidacaoCampo = verificarCamposVazios(novoMembro)
            if (validacao.erro) {
                setModalVisible(!modalVisible)
                Alert.alert('Erro ao cadastrar', validacao.mensagem);
            }
            firebase.database().ref(`tb_membro/${celula?.id}`).push(novoMembro).then(response => {
                Alert.alert(`Cadastro realizado`, `${novoMembro.apelido} cadastrado com sucesso`,)
            })
            firebase.database().ref(`tb_membro/${celula?.id}`).on('value', membrosResp => {
                let mem: MembroModel[] = [];
                membrosResp.forEach(elem => {
                    mem.push({ id: elem.key, ...elem.val() })
                })
                AsyncStorage.setItem('membros', JSON.stringify(mem))
            });
        }
    };

    function novoIntegrante() {
        setMembroSelecionado({});
        setTextTelefone('');
        setTextNome('');
        setTextApelido('');
        setTextTelefone('');
        setTextDateString('');
    }

    function verificarCamposVazios(memb: MembroModel): ValidacaoCampo {
        var validacao: ValidacaoCampo = {}
        if (!memb.apelido || memb.apelido == '') {
            validacao = { erro: true, mensagem: `campo APELIDO em branco` };
            return validacao;
        }
        if (!memb.nome || memb.nome == '') {
            validacao = { erro: true, mensagem: `campo NOME em branco` };
            return validacao;
        }
        if (!memb.telefone || memb.telefone == '') {
            validacao = { erro: true, mensagem: `campo TELEFONE em branco` };
            return validacao;
        }
        validacao = { erro: false, mensagem: `` };
        return validacao
    }

    function enviarMensagem(mem: MembroModel) {
        Linking.openURL(`whatsapp://send?phone=+55${mem.telefone}`)
    }

    useEffect(() => {
        consultarStorage();
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <PageHeader title={celula?.nome} />

            <View style={styles.header}>
                <Text style={styles.headerTitle}>Integrantes da célula:</Text>
            </View>

            <ScrollView>
                <View style={styles.bodyCard}>
                    {membros.map((membro, index) => {
                        return (
                            <View key={membro.id} style={styles.styleCardMembro}>
                                <View>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Nome: </Text>
                                        {membro.nome}
                                    </Text>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Apelido: </Text>
                                        {membro.apelido}
                                    </Text>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Data Aniversario: </Text>
                                        {dataAniversario(membro.dataNascimento as string)}
                                    </Text>
                                </View>
                                <View style={styles.containerButton}>
                                    <TouchableOpacity style={styles.buttonWhastApp} onPress={() => {enviarMensagem(membro)}}>
                                        <FontAwesome5 name="whatsapp" size={23} color="#fff" />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.buttonEdit} onPress={() => { setModalVisible(true), editarMembro(membro) }}>
                                        <AntDesign name="edit" size={24} color="#fff" />
                                    </TouchableOpacity>
                                </View>
                            </View>

                        )
                    })}
                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.footerButton} onPress={() => { novoIntegrante(), setModalVisible(true) }}>
                            <AntDesign name="addusergroup" size={26} color="#fff" />
                            <Text style={styles.footerButtonText}>Novo Integrantes</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView>

            <Modal
                animationType='slide'
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalBody}>
                        <Text style={styles.modalTitle}>{membroSelecionado.id ? 'Editar informações' : 'Novo Integrante'}</Text>

                        <View style={styles.modalForm}>
                            <Text style={styles.textInputTitle}>Nome: </Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={textNome}
                                onChangeText={(valor) => { setTextNome(valor) }}
                            />

                            <Text style={styles.textInputTitle}>Apelido: </Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={textApelido}
                                onChangeText={(valor) => { setTextApelido(valor) }}
                            />

                            <Text style={styles.textInputTitle}>Telefone: </Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={textTelefone}
                                onChangeText={(valor) => { setTextTelefone(valor) }}
                            />

                            <Text style={styles.textInputTitle}>Data Nascimento: </Text>
                            <DatePicker
                                style={styles.modalDatePicker}
                                date={textDateString}
                                mode='date'
                                format='YYYY-MM-DD'
                                // format='DD-MM-YYYY'
                                customStyles={{
                                    dateInput: styles.modalFormInputDatePiker
                                }}
                                onDateChange={(dateValue) => { setTextDateString(dateValue) }}
                            />
                        </View>

                        <View style={styles.modalFooter}>

                            <TouchableOpacity style={styles.modalButtonFechar}
                                onPress={() => { setModalVisible(!modalVisible) }}
                            >
                                <AntDesign style={{ marginRight: 10 }} name="close" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    Fechar
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.modalButtonEnviar}
                                onPress={() => { setModalVisible(!modalVisible), salvarMembro() }}
                            >
                                <AntDesign style={{ marginRight: 10 }} name="save" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    {membroSelecionado.id ? 'Salvar' : 'Cadastrar'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default Membros;