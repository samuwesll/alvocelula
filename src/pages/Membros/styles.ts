import { StyleSheet } from "react-native";
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
    header: {
        margin: 10,
        alignItems: 'center',
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 18,
    },
    styleCardMembro: {
        flexDirection: 'row',
        // alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17,
        justifyContent: 'space-between',
        width: '90%'
    },
    bodyCard: {
      alignItems: 'center',
    },
    styleCardMembroText: {
        // fontWeight: 'bold',
        margin: 3,
    },
    styleCardMembroTextTitle: {
        fontWeight: 'bold',
    },
    containerButton: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonWhastApp: {
        backgroundColor: 'green',
        margin: 5,
        padding: 8,
        borderRadius: 16,
    },
    buttonEdit: {
        backgroundColor: 'orange',
        margin: 5,
        padding: 8,
        borderRadius: 16,
    },
    modalContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 40,
    },
    modalBody: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: '85%'
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 18
    },
    modalForm: {
        width: '90%',
        marginBottom: 20,
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '90%'
    },
    modalButtonFechar: {
        backgroundColor: "red",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    },
    modalButtonEnviar: {
        backgroundColor: "green",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    },
    textInputTitle: {
        fontSize: 12,
        marginBottom: -2,
        marginLeft: 5,
        backgroundColor: '#ffff'
    },
    modalFormInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 8,
        paddingHorizontal: 24,
        fontSize: 16,
        marginVertical: 5,
    },
    modalFormInputDatePiker: {
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 8,
        paddingHorizontal: 24,
        fontSize: 16,
        marginVertical: 5,
        alignItems: 'flex-start'
    },
    modalDatePicker: {
        width: '100%',
        marginTop: 10,
    },
    footer: {
        marginVertical: 20,
    },
    footerButton: {
        backgroundColor: 'black',
        padding: 7,
        borderRadius: 8,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
    },
    footerButtonText: {
        color: '#ffff',
        marginLeft: 10,
        fontWeight: '700'
    }
})

export default styles;