import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, Picker, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import firebase from '../../service/firebase';

import styles from './styles';
import PageHeader from '../../components/PageHeader';
import Calendario from '../../components/Calendario';

function Dashboard() {

    const navigation = useNavigation();

    const [celula, setCelula] = useState<CelulaModel>();
    const [relatorio, setRelatorio] = useState<RelatorioCelula[]>([]);
    const [listaDataRel, setListaDataRel] = useState<string[]>([]);
    const [mesesInicioQtd, setMesesInicioQtd] = useState<number>();

    function consultaAsyncStorage() {
        let login: LiderModel = {}
        AsyncStorage.getItem('login').then(response => {
            login = JSON.parse(response as any) as LiderModel;
            if (!response) {
                return navigation.navigate('Login')
            }
        })
        setTimeout(() => {
            consultaFirebase(login.arrayCelula?.values().next().value);
        }, 300)
    }

    async function consultaFirebase(idCelula: string) {
        firebase.database().ref(`tb_celula/${idCelula}`).on('value', response => {
            setCelula({ id: response.key, ...response.val() })
            setMesesInicioQtd(calcularMeses({ id: response.key, ...response.val() }))
        })
        firebase.database().ref(`tb_relatorio_celula/${idCelula}`).on('value', relat => {
            const arrayData: string[] = []
            const relatorios: RelatorioCelula[] = [];
            relat.forEach(ele => {
                arrayData.push(ele.key as string);
                relatorios.push({ id: ele.key, ...ele.val() });
            })
            setListaDataRel(arrayData)
            AsyncStorage.setItem('relatorio', JSON.stringify(relatorios))
        });
    }

    function calcularMeses(cel: CelulaModel): number {
        const dataAtual: Date = new Date();
        let mesInicio = new Date(cel.dataInicio as string).getMonth();
        let anoInicio = new Date(cel.dataInicio as string).getFullYear();
        let mesAtual = dataAtual.getMonth();
        let anoAtual = dataAtual.getFullYear();
        return ((mesAtual + 12 * anoAtual) - (mesInicio + 12 * anoInicio) + 1)
    }

    useEffect(() => {
        consultaAsyncStorage();
    }, []);

    useEffect(() => {
    }, [relatorio])

    return (
        <View style={styles.container}>
            <Calendario celulaObj={celula} listaDataRel={listaDataRel} qtdMeses={mesesInicioQtd} />
        </View>
    )
}

export default Dashboard;