import { StyleSheet } from "react-native";
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
})

export default styles;