import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    carrregamento: {
        flex: 1,
        justifyContent: 'center'
    },
    keyboard: {
        flex: 1,
    },
    container: {
        // flex: 1,
        padding: 32,
    },
    main: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 300,
    },
    title: {
        color: '#322153',
        fontSize: 22,
        marginBottom: 1,
        fontFamily: 'Roboto_400Regular',
        fontWeight: 'bold'
    },
    description: {
        fontSize: 18,
        color: '#6C6C80',
        fontFamily: 'Roboto_500Medium',
        marginTop: 10,
    },
    footer: {

    },
    input: {
        height: 60,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 8,
        paddingHorizontal: 24,
        fontSize: 16,
    },
    button: {
        backgroundColor: 'black',
        height: 60,
        flexDirection: 'row',
        borderRadius: 10,
        overflow: 'hidden',
        alignItems: 'center',
        marginTop: 8,
    },
    buttonIcon: {
        height: 60,
        width: 60,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        justifyContent: 'center',
        alignItems: 'center',
        borderRightColor: '#fff'
    },
    buttonText: {
        flex: 1,
        justifyContent: 'center',
        textAlign: 'center',
        color: '#FFF',
        fontSize: 16,
    }
});

export default styles;