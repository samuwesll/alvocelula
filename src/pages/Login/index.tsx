import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Platform, ImageBackground, Image, TextInput, ActivityIndicator, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '../../service/firebase';

import fundoImage from '../../assets/images/fundo_logo.png';
import logoImage from '../../assets/images/logo.png';
import styles from './style';
import { ResponseAuth } from '../../models/ResponseAuth';

function Login() {

    const [name, setName] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    const [carregando, setCarregando] = useState<boolean>(true)

    const navigation = useNavigation();

    async function realizarLogin(login: string, senha: string) {
        let retornoAuth: ResponseAuth = {};
        let use: LiderModel = {};
        if (login.length <= 5 && senha.length <= 5) {
            return Alert.alert('Validação dos campos', 'Os campos preenchidos devem conter pelo menos 5 caracteres!')
        };
        await firebase.auth().signInWithEmailAndPassword(login.concat('@alvo.com'), senha).then(response => {
            retornoAuth = response.user?.toJSON() as ResponseAuth;
            if (retornoAuth.displayName != 'PageLider' && retornoAuth.displayName) {
                setName('')
                setPassword('')
                Alert.alert('Permisão negada', 'Login informado deve ser realizado via web')
            }
            firebase.database().ref(`user/${retornoAuth.uid}`).on('value', response => {
                use = { id: response.key, ...response.val() }
                AsyncStorage.setItem('login', JSON.stringify(use));
            })
        }).catch(() => {
            return Alert.alert('Login ou senha invalido', 'Login ou senha informado incorreto')
        })
        if (retornoAuth.displayName != 'PageLider' && retornoAuth.displayName) {
            setName('')
            setPassword('')
            Alert.alert('Permisão negada', 'Login informado deve ser realizado via web')
        }
        firebase.database().ref(`user/${retornoAuth.uid}`).on('value', response => {
            use = { id: response.key, ...response.val() }
            AsyncStorage.setItem('login', JSON.stringify(use));
        })


        setTimeout(() => {
            if (retornoAuth.displayName == 'PageLider') {
                consultandoEvento();
                AsyncStorage.setItem('secrets', JSON.stringify({ login: name, senha: password }))
                setName('');
                setPassword('')
                navigation.navigate(retornoAuth.displayName as string);
            }
        }, 300)

    }

    function consultandoEvento() {
        //Evento Semanal
        const eventoSemanal: EventoSemanalModel[] = [];
        firebase.database().ref('tb_evento/semanal').on('value', result => {
            result.forEach(evtSem => {
                eventoSemanal.push({ id: evtSem.key, ...evtSem.val() })
            })
        });
        AsyncStorage.setItem('eventoSemanal', JSON.stringify(eventoSemanal));

        //Evento Mensal
        const eventoMensal: EventoMensalModel[] = [];
        firebase.database().ref('tb_evento/mensal').on('value', result => {
            result.forEach(elem => {
                eventoMensal.push({ id: elem.key, ...elem.val() })
            })
        })
        AsyncStorage.setItem('eventoMensal', JSON.stringify(eventoMensal));
    }

    useEffect(() => {
        let use: LiderModel = {};
        let credency: ResponseAuth = {};
        let secrets: Secrets = {};
        AsyncStorage.clear()
        consultandoEvento();
        AsyncStorage.getItem('secrets').then(response => {
            secrets = JSON.parse(response as any);
        })

        setTimeout(async () => {
            if (secrets) {
                setCarregando(false)
                await firebase.auth().signInWithEmailAndPassword(secrets.login?.concat('@alvo.com') as string, secrets.senha as string).then(response => {
                    credency = response.user?.toJSON() as ResponseAuth;
                })
                firebase.database().ref(`user/${credency.uid}`).on('value', response => {
                    use = { id: response.key, ...response.val() }
                    AsyncStorage.setItem('login', JSON.stringify(use));
                })
                setCarregando(true)
                navigation.navigate(credency.displayName as string);
            }
        }, 500)
    }, [])

    if (carregando == false) {
        return <View style={styles.carrregamento}>
            <ActivityIndicator size="large" color="black" />
        </View>
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <ImageBackground
                source={fundoImage}
                imageStyle={{ width: 274, height: 368 }}
                style={styles.container}
            >

                <View style={styles.main}>
                    <Image source={logoImage} />
                    <Text style={styles.title}>Uma igreja para quem pensa.</Text>
                    <Text style={styles.description}>Relatório de célula - Itapecerica</Text>
                </View>

                <View style={styles.footer}>
                    <TextInput style={styles.input} value={name} placeholder='Digite seu login' onChangeText={setName} maxLength={20} />
                    <TextInput secureTextEntry={true} value={password} style={styles.input} placeholder='Digite senha' onChangeText={setPassword} maxLength={30} />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => realizarLogin(name, password)}
                    >
                        <View style={styles.buttonIcon}>
                            <Feather name="arrow-right" color="#fff" size={24} />
                        </View>
                        <Text style={styles.buttonText}>Entrar</Text>
                    </TouchableOpacity>
                </View>

            </ImageBackground>
        </ScrollView>
    )
}

export default Login;