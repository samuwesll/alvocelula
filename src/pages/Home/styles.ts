import { StyleSheet } from "react-native";
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
    header: {
        backgroundColor: '#5a5a60',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    headerTextTitle: {
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: 20,
        marginTop: 5,
        marginRight: 10,
    },
    nomeLider: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#ffff'
    },
    lider: {
        color: '#fff'
    },
    headerViewImage: {
        borderRadius: 40,
        margin: 5,
        width: 50,
        height: 50,
    },
    descricaoCelula: {
        alignSelf: 'center',
        // marginTop: 5,
        width: '95%',
        display: "flex",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 2,
        padding: 10,
        // borderWidth: 1,
        // borderColor: "#dcdc",
        // borderRadius: 6,
    },
    mesAno: {
        width: '40%',
        fontWeight: 'bold',
    },

    infoCelula: {
        marginTop: 5,
        marginBottom: 10,
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
    infoPart: {
        alignItems: 'center',
        width: '30%',
        borderWidth: 1,
        borderColor: "#dcdc",
        borderRadius: 6,
    },
    infoPartTitle: {
        fontWeight: 'bold',
    },
    infoPartText: {
        marginTop: 3,
        marginBottom: 5,
    },
    detalhesCelulas: {
        marginTop: 20,
        padding: 10,
        backgroundColor: '#ffff',
        borderRadius: 16,
    },
    detalhesCelulasHeader: {
        alignItems: 'center',
        marginBottom: 10,
    },
    detalhesCelulasHeaderText: {
        fontSize: 16,
        fontFamily: 'Roboto_500Medium'
    }
})

export default styles;