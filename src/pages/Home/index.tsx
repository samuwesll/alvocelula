import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, Picker, SafeAreaView } from 'react-native';
import firebase from '../../service/firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import 'intl';
import "intl/locale-data/jsonp/pt-BR";

import Chart from '../../components/Chart'
import PageHeader from '../../components/PageHeader';

import styles from './styles';
import { AppLoading } from 'expo';

interface HomeProps {

}

const Home: React.FC<HomeProps> = () => {

    // const navigation = useNavigation();
    const [mesAno, setMesAno] = useState<string[]>([])
    const [mesAno2, setMesAno2] = useState<string>()

    const [celula, setCelula] = useState<CelulaModel>();
    const [membros, setMembros] = useState<Array<MembroModel>>([]);
    const [membrosFilter, setMembrosFilter] = useState<Array<MembroModel>>([]);

    const [visitantes, setVisitantes] = useState<number>();
    const [qtdMembros, setQtdMembros] = useState<number>();
    const [qtdKgAmor, setQtdKgAmor] = useState<number>();
    const [qtdOfertas, setQtdOfertas] = useState<number>();
    const [infoSemana, setInfoSemanas] = useState<any[]>([]);
    const [infoSemanaIndex, setInfoSemanasIndex] = useState<any[]>([]);

    const [relatorio, setRelatorio] = useState<RelatorioCelula[]>([])

    function formatarData(data: string): string {
        const date: Date = new Date(new Date(data).setDate(new Date(data).getDate() + 1))
        const dia = date.getDate().toString().padStart(2, '0');
        const mes = (date.getMonth() + 1).toString().padStart(2, '0');
        return dia + '/' + mes;
    };

    function calcularMeses(): number {
        const dataAtual: Date = new Date();
        let mesInicio = new Date(celula?.dataInicio as string).getMonth();
        let anoInicio = new Date(celula?.dataInicio as string).getFullYear();
        let mesAtual = dataAtual.getMonth();
        let anoAtual = dataAtual.getFullYear();
        return ((mesAtual + 12 * anoAtual) - (mesInicio + 12 * anoInicio) + 1) - 1
    }

    function avançarMes() {
        const listaData: Array<string> = [];
        const qtsMeses = calcularMeses();
        const monthNamesShort: Array<string> = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        const dataInicioMaisUm = new Date(new Date(celula?.dataInicio as string).setDate(new Date(celula?.dataInicio as string).getDate() + 1))
        for (let i = 0; i < qtsMeses; i++) {
            var data = new Date(dataInicioMaisUm).setMonth(new Date(dataInicioMaisUm).getMonth() + i);
            var indiceMes = new Date(data).getMonth();
            var mes = monthNamesShort[indiceMes];
            var ano = new Date(data).getFullYear();
            listaData.push(mes + "-" + ano)
        };
        setMesAno(listaData.reverse());
        setMesAno2(mesAno[mesAno.length]);
    };

    function consultaFirebase(login: LiderModel) {
        const idCelula: string = login.arrayCelula?.values().next().value;
        firebase.database().ref(`tb_celula/${idCelula}`).on('value', response => {
            setCelula({ id: login.arrayCelula?.values().next().value, ...response.val() })
        })
        firebase.database().ref(`tb_relatorio_celula/${idCelula}`).on('value', response => {
            let relatorioResp: RelatorioCelula[] = [];
            response.forEach(resp => {
                relatorioResp.push({ id: resp.key, ...resp.val() })
            });
            setRelatorio(relatorioResp)
        })
        firebase.database().ref(`tb_membro/${idCelula}`).on('value', response => {
            const membrosFirebase: MembroModel[] = [];
            response.forEach(resp => {
                membrosFirebase.push({ id: resp.key, ...resp.val() })
            });
            setMembros(membrosFirebase)
        })
    }

    function consultaStorage() {
        let login: LiderModel = {}
        AsyncStorage.getItem('login').then(response => {
            login = JSON.parse(response as any) as LiderModel;
        })
        setTimeout(() => {
            consultaFirebase(login)
        }, 300)
    };

    function verificandoDataSelecionado(): number {
        const qtdMeses = mesAno.indexOf(mesAno2 as string, 1) < 0 ? 0 : mesAno.indexOf(mesAno2 as string, 1);
        const dataInicioMaisUm = new Date(new Date(celula?.dataInicio as string).setDate(new Date(celula?.dataInicio as string).getDate() + 1))
        const data = new Date(dataInicioMaisUm).setMonth((new Date(dataInicioMaisUm).getMonth() + calcularMeses()) - qtdMeses - 1);
        return data;
    };

    async function filtrandoRelatorio(rel: RelatorioCelula[]) {
        let vis: number = 0;
        let mem: string[] = [];
        let kgAmor: number = 0;
        let ofertas: number = 0;
        let freq: any[] = [];
        let freqIndex: any[] = [];
        await rel.forEach((result, index) => {
            vis += result.visitantes as number;
            kgAmor += result.quiloAmor as number;
            ofertas += result.valorOferta as number;
            result.idMembrosPresentes?.forEach(r => { mem.push(r) })
            freq.push([result.visitantes as any, result.idMembrosPresentes?.length])
            freqIndex.push([`${index + 1} sem.`])
        })
        setVisitantes(vis)
        setQtdKgAmor(kgAmor)
        setQtdOfertas(ofertas)
        setInfoSemanas(freq)
        setInfoSemanasIndex(freqIndex)
        const filterQtdMembros = await mem.filter((ele, index, self) => {
            return index === self.indexOf(ele);
        })
        setQtdMembros(filterQtdMembros.length)
    }

    function mascaraValor(valor: number) {
        return Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valor);
    }


    useEffect(() => {
        consultaStorage();
    }, [])

    useEffect(() => {
        avançarMes();
    }, [celula])

    useEffect(() => {
        const data = verificandoDataSelecionado()
        const mes = new Date(data).getMonth();
        const ano = new Date(data).getFullYear();
        var consultinha = membros.filter((membro, index) => {
            return new Date(membro.dataNascimento as string).getMonth() == mes;
        })
        setMembrosFilter(consultinha)
        let result = relatorio.filter((relat, index) => {
            let mesSelecionado = new Date(verificandoDataSelecionado()).getMonth()
            return new Date(relat.data as string).getMonth() == mesSelecionado;
        });
        filtrandoRelatorio(result);
    }, [mesAno2])

    if (mesAno == [] || !mesAno2) {
        <AppLoading />
    }

    return (
        <SafeAreaView style={styles.container}>
            <PageHeader title={celula?.nome} />

            <ScrollView>
                <View style={styles.descricaoCelula}>
                    <View style={{ alignItems: 'flex-end' }}>
                        <Picker
                            selectedValue={mesAno2}
                            style={styles.mesAno}
                            onValueChange={(mesValue, mesIndex) => setMesAno2(mesValue)}
                        >
                            {mesAno.map(item => (
                                <Picker.Item
                                    key={item}
                                    label={item}
                                    value={item}
                                />
                            ))}
                        </Picker>
                    </View>

                    <Chart semanas={infoSemana as any} semanasIndex={infoSemanaIndex as any} />

                    <View style={styles.detalhesCelulas}>

                        <View style={styles.detalhesCelulasHeader}>
                            <Text style={styles.detalhesCelulasHeaderText}>Detalhes:</Text>
                        </View>

                        <View style={styles.infoCelula}>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Visitantes:</Text>
                                <Text style={styles.infoPartText}>{visitantes?.toString().padStart(2, '0')}</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Membros:</Text>
                                <Text style={styles.infoPartText}>{qtdMembros?.toString().padStart(2, '0')}</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Total Pessoas</Text>
                                <Text style={styles.infoPartText}>{(visitantes as any + qtdMembros as any).toString().padStart(2, '0')}</Text>
                            </View>
                        </View>
                        <View style={styles.infoCelula}>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Kg do amor:</Text>
                                <Text style={styles.infoPartText}>{qtdKgAmor?.toFixed(3)} kg</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Ofertas:</Text>
                                <Text style={styles.infoPartText}>{mascaraValor(qtdOfertas as number)}</Text>
                            </View>
                        </View>

                    </View>

                    <View style={styles.detalhesCelulas}>

                        <View style={styles.detalhesCelulasHeader}>
                            <Text style={styles.detalhesCelulasHeaderText}>Aniversariantes do mês:</Text>
                        </View>

                        {membrosFilter.map((membro: MembroModel, index) => {
                            return (
                                <View key={membro.id} style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text>
                                        <Text style={{ fontWeight: 'bold' }}>Apelido: </Text>
                                        {membro.apelido}
                                    </Text>
                                    <Text>
                                        <Text style={{ fontWeight: 'bold' }}>Data: </Text>
                                        {formatarData(membro.dataNascimento as string)}
                                    </Text>
                                </View>
                            )
                        })}


                    </View>
                </View>

            </ScrollView>
        </SafeAreaView >
    )
}

export default Home;