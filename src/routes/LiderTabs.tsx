import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons'
import Constants from 'expo-constants';

import Home from '../pages/Home';
import Membros from '../pages/Membros';
import Dashboard from '../pages/Dashboard';
import Ajuste from '../pages/Ajuste';

const { Navigator, Screen } = createBottomTabNavigator();

function LiderTabs() {
    return (
        <Navigator
            tabBarOptions={{
                style: {
                    elevation: 0,
                    shadowOpacity: 0,
                    height: 56,
                },
                tabStyle: {
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 8,
                    margin: 4,
                },
                iconStyle: {
                    flex: 0,
                    width: 20,
                    height: 30,
                },
                labelStyle: {
                    fontSize: 13,
                    fontWeight: 'bold',
                },
                // activeBackgroundColor: '#5a5a60',
                activeTintColor: 'black'
            }}
        >
            <Screen 
                name="Home" 
                component={Home}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="home" size={24} color={color} />
                    }}}
            />
            <Screen 
                name="Relatórios" 
                component={Dashboard}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="piechart" size={24} color={color} />
                    }}}
            />
            <Screen 
                name="Membros" 
                component={Membros}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="team" size={24} color={color} />
                    }}}
            />
            <Screen 
                name="Usuario" 
                component={Ajuste}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="solution1" size={24} color={color} />
                    }}}
            />
        </Navigator>
    )
}

export default LiderTabs;