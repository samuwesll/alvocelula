import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../pages/Login';
import LiderTabs from './LiderTabs';

const { Navigator, Screen } = createStackNavigator();

function AppStack() {
    return (
        <NavigationContainer>
            <Navigator
                headerMode="none"
                screenOptions={{
                    cardStyle: {
                        backgroundColor: "#f0f0f5"
                    }
                }}
            >
                <Screen name="Login" component={Login}/>
                <Screen name="PageLider" component={LiderTabs}/>
            </Navigator>
        </NavigationContainer>
    )
}

export default AppStack;