import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Modal, Alert, AsyncStorage, TextInput, ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import firebase from '../../service/firebase'
import { Feather } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

import EventoProps from '../../models/EventoProps';

import styles from './style';
import Membros from '../../pages/Membros';

interface EventoCelulaProps {
    evento: EventoProps,
    celula?: CelulaModel,
    membros?: MembroModel[],
}

const EventoCelula: React.FC<EventoCelulaProps> = ({ evento, celula, membros }) => {

    const [modalVisible, setModalVisible] = useState(false);
    const [user, setUser] = useState<LiderModel>();
    const [relatorio, setRelatorio] = useState<RelatorioCelula>();
    const [arrayMembros, setArrayMembros] = useState<MembroModel[]>();
    // const [arrayMembros, setArrayMembros] = useState<MembroModel[]>(membros as MembroModel[]);
    const [selectedItems, setSelectedItems] = useState<string[]>([]);

    const [visitante, setVisitantes] = useState<string>('');
    const [oferta, setOferta] = useState<string>('');
    const [quiloAmor, setQuiloAmor] = useState<string>('');

    function consultaLogin() {
        AsyncStorage.getItem('login').then(result => {
            setUser(JSON.parse(result as any))
        })
    }
    useEffect(() => {
        consultaLogin();
    }, [])

    async function handleRelatorioCelula() {
        setRelatorio({
            idLider: user?.liderId,
            dataEnvio: new Date().toISOString(),
            data: evento.dia,
            idMembrosPresentes: selectedItems,
            quiloAmor: Number.parseFloat(quiloAmor),
            visitantes: Number.parseInt(visitante),
            valorOferta: Number.parseFloat(oferta),
        })
        await firebase.database().ref(`tb_relatorio_celula/${celula?.id}/${evento.dia}`).set(relatorio).then(resp => {
            Alert.alert(`Relatorio enviado com sucesso`);
            setModalVisible(!modalVisible)
            setOferta('');
            setQuiloAmor('')
            setVisitantes('')
        })
    }

    function handleSelectItem(id: string) {
        const alreadySelected = selectedItems.findIndex(item => item == id)
        
        if(alreadySelected >= 0) {
            const filteredItems = selectedItems.filter(item => item !== id);
            setSelectedItems(filteredItems);
        } else {
            setSelectedItems([...selectedItems, id])
        }
    }

    function editarRelatorio(evento: EventoProps) {
        let relatorioResp: RelatorioCelula = {};
        firebase.database().ref(`tb_relatorio_celula/${celula?.id}/${evento.dia}`).on('value', result => {
            relatorioResp = {id: result.key, ...result.val()} as RelatorioCelula
        })
        setOferta(relatorioResp.valorOferta as any)
        setQuiloAmor(relatorioResp.quiloAmor as any)
        setVisitantes(relatorioResp.visitantes as any)
        setSelectedItems(relatorioResp.idMembrosPresentes as any)
    }

    function enviarRelatorio(evento: EventoProps) {
        setOferta('')
        setQuiloAmor('')
        setVisitantes('')
        setSelectedItems([])
    }

    function consultandoNovosIntegrantes() {
        firebase.database().ref(`tb_membro/${celula?.dia}`).on('value', response => {
            let membros: MembroModel[] = [];
            response.forEach(resp => {
                membros.push({id: resp.key, ...resp.val()})
            })
            setArrayMembros(membros);
        })
    }

    useEffect(() => {
        // consultandoNovosIntegrantes()
        setArrayMembros(membros)
    }, [enviarRelatorio, editarRelatorio])
    
    return (
        <View style={styles.container}>

            <View style={styles.evento}>
                <Text style={styles.eventoText}>
                    <Text style={styles.eventoTextTitle}>Célula: </Text>
                    {evento.nome}
                </Text>
                <Text style={styles.eventoText}>
                    <Text style={styles.eventoTextTitle}>Hórario: </Text>
                    {evento.horario}
                </Text>
                <Text style={styles.eventoText}>
                    <Text style={styles.eventoTextTitle}>Perfil: </Text>
                    {celula?.perfil}
                </Text>
                {evento.relatorio
                    ? <Text style={styles.eventoTextOk}>
                        <Text style={styles.eventoTextTitle}>Relatorio: </Text>
                        Entregue
                    </Text>
                    : <Text style={styles.eventoTextPendente}>
                        <Text style={styles.eventoTextTitle}>Relatorio: </Text>
                        Pendente
                    </Text>
                }
            </View>

            {(evento.celula)
                ? (!evento.relatorio) ?
                    <View>
                        <TouchableOpacity style={styles.buttonForm} onPress={() => { setModalVisible(true), enviarRelatorio(evento) }}>
                            <AntDesign name="form" size={20} color="#fff" />
                            <Text style={{color: '#ffff', marginLeft: 10,}}>Preencher</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View>
                        <TouchableOpacity style={styles.buttonEdit} onPress={() => { setModalVisible(true), editarRelatorio(evento) }}>
                            <AntDesign name="edit" size={20} color="#fff" />
                            <Text style={{color: '#ffff', marginLeft: 10,}}>Editar</Text>
                        </TouchableOpacity>
                    </View>
                : null
            }

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalBody}>
                        <Text style={styles.modalTitle}>Prencher relatório</Text>

                        <View style={styles.modalForm}>
                            <Text style={styles.textInputTitle}>Valor Oferta:</Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={oferta.toString()}
                                // placeholder='Valor Oferta'
                                onChangeText={(value) => { setOferta(value) }}
                                keyboardType='numeric'

                            />
                            <Text style={styles.textInputTitle}>Quilo do amor:</Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={quiloAmor.toString()}
                                // placeholder='Quilo do amor'
                                onChangeText={(value) => { setQuiloAmor(value) }}
                                keyboardType='numeric'

                            />
                            <Text style={styles.textInputTitle}>Qtd visitantes:</Text>
                            <TextInput
                                style={styles.modalFormInput}
                                value={visitante.toString()}
                                // placeholder='Qtd visitantes'
                                onChangeText={(value) => { setVisitantes(value) }}
                                keyboardType='numeric'

                            />

                            <Text style={styles.listaMembrosTitle}>Selecione membros presentes:</Text>

                            <ScrollView
                                horizontal
                                // showsHorizontalScrollIndicator={false}
                                style={styles.listaItemScroll}
                            >
                                {arrayMembros?.map((elem, index) => {
                                    return (
                                        <TouchableOpacity 
                                            key={elem.id} 
                                            style={[
                                                styles.listaMembrosButton,
                                                selectedItems.includes(elem.id as string) ? styles.selectedItem : {}
                                            ]} 
                                            onPress={() => handleSelectItem(elem.id as string)}
                                        >
                                            {/* <Feather name="user-check" size={24} color="#ffff" /> */}
                                            { selectedItems.includes(elem.id as string) ? (
                                                <Feather name="user-check" size={24} color="#ffff" />
                                            ) : (
                                                <Feather name="user-x" size={24} color="black" />
                                            )}
                                            <Text
                                                style={[
                                                    styles.ItemText,
                                                    selectedItems.includes(elem.id as string) ? styles.selectedItemText : {}
                                                ]}
                                            >
                                                {elem.apelido}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>

                        </View>

                        <View style={styles.modalFooter}>

                            <TouchableOpacity style={styles.modalButtonFechar}
                                onPress={() => {
                                    setModalVisible(!modalVisible);
                                }}
                            >
                                <AntDesign style={{ marginRight: 10 }} name="close" size={18} color="white" />
                                <Text style={{
                                    color: "white",
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    marginRight: 5,
                                }}>
                                    Fechar
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.modalButtonEnviar}
                                onPress={() => {
                                    handleRelatorioCelula()
                                }}
                            >
                                <AntDesign style={{ marginRight: 10 }} name="rocket1" size={18} color="white" />
                                <Text style={{
                                    color: "white",
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    marginRight: 5,
                                }}>
                                    Enviar
                                </Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>

        </View>
    )
}

export default EventoCelula;