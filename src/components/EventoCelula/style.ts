import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17,
        justifyContent: 'space-around'
    },
    evento: {
        // marginTop: 20,
        width: '75%',
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    eventoText: {
        margin: 2,
    },
    eventoTextOk: {
        color: 'green'
    },
    eventoTextPendente: {
        color: 'red'
    },
    eventoTextTitle: {
        fontWeight: 'bold',
        color: 'black'
    },
    buttonEdit: {
        backgroundColor: 'orange',
        padding: 5,
        borderRadius: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginRight: 10,
    },
    buttonForm: {
        backgroundColor: 'black',
        padding: 5,
        borderRadius: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginRight: 10,
    },
    modalContainer: {
        // flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 60,
    },
    modalBody: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: '80%'
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 18
    },
    modalButtonFechar: {
        backgroundColor: "red",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    },
    modalButtonEnviar: {
        backgroundColor: "green",
        borderRadius: 20,
        padding: 7,
        elevation: 2,
        flexDirection: 'row'
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '90%'
    },
    modalForm: {
        width: '90%',
        marginBottom: 20,
    },
    modalFormInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 8,
        paddingHorizontal: 24,
        fontSize: 16,
        marginVertical: 5,
    },
    listaMembros: {
        // width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        // flexWrap: 'wrap',
    },
    listaItemScroll: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    listaMembrosTitle: {
        textAlign: 'center',
        margin: 15,
    },
    listaMembrosButton: {
        margin: 10,
        backgroundColor: '#d3d3d3',
        padding: 10,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedItem: {
        backgroundColor: '#4169E1',
    },
    ItemText: {
        fontFamily: 'Caladea_400Regular_Italic',
        fontSize: 16,
    },
    selectedItemText: {
        color: '#ffff'
    },
    textInputTitle: {
        fontSize: 10,
        marginBottom: -2,
        marginLeft: 5,
        backgroundColor: '#ffff'
    }
})
export default styles;