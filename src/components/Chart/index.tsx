import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { StackedBarChart } from 'react-native-chart-kit'

class Chart extends React.PureComponent {

    props = {
        semanas: [],
        semanasIndex: [],
    }

    data = {
        labels: ["1 sem.", "2 sem.", "3 sem.", "4 sem.",],
        legend: ["visitantes", "membros"],
        data: [],
        barColors: ["#dfe4ea", "#ced6e0"]
    };


    render() {
        const teste =() => {
            this.data.labels = this.props.semanasIndex;
            this.data.data = this.props.semanas;
        };
        teste();
        return (
            <View>
                {/* <Text>Participação</Text> */}
                <StackedBarChart
                    style={{ borderRadius: 16}}
                    data={this.data}
                    hideLegend={false}
                    withVerticalLabels
                    withHorizontalLabels={false}
                    width={350}
                    height={220}
                    chartConfig={{
                        backgroundGradientFrom: "#ffff",
                        backgroundGradientFromOpacity: 1,
                        backgroundGradientTo: "",
                        backgroundGradientToOpacity: 0.1,
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        strokeWidth: 2, // optional, default 3
                        barPercentage: 0.7,
                        useShadowColorFromDataset: false, // optional
                        paddingTop: 15,
                    }}
                />
            </View>
        )
    }

}

export default Chart