import React from 'react';
import { View, Text } from 'react-native';

import styles from './style';

interface EventoProps {
    eventoSemanal?: EventoSemanalModel,
    eventoMensal?: EventoMensalModel,
}

const Evento: React.FC<EventoProps> = ({ eventoSemanal }) => {
    return (
        <View style={styles.container}>
            {eventoSemanal?.id ?
                <View style={styles.evento}>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Evento: </Text>
                        {eventoSemanal.nome}
                    </Text>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Horário: </Text>
                        {eventoSemanal.horario}
                    </Text>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Local: </Text>
                        {eventoSemanal.local}
                    </Text>
                </View>
                :
                null
            }
        </View>
    )
}

export default Evento;