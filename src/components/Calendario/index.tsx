import React, { useEffect, useState } from 'react';
import { LocaleConfig, Agenda } from 'react-native-calendars';
import { View, Text, AsyncStorage, ActivityIndicator } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import firebase from '../../service/firebase';
import { AppLoading } from 'expo';

import EventoCelula from '../EventoCelula';
import styles from './style'
import EventoProps from '../../models/EventoProps';
import Evento from '../Evento';

interface CalendarioProps {
    celulaObj?: CelulaModel,
    relatorio?: RelatorioCelula[];
    listaDataRel?: string[];
    qtdMeses?: number,
}

const Calendario: React.FC<CalendarioProps> = ({ celulaObj, listaDataRel, qtdMeses }) => {

    LocaleConfig.locales['br'] = {
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan.', 'Fev.', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul.', 'Ago', 'Set.', 'Out.', 'Nov.', 'Dez.'],
        dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
    };
    LocaleConfig.defaultLocale = 'br';

    const [items, setItems] = useState<Object>({});
    const [marked, setMarked] = useState<Object>({});
    const [membros, setMembros] = useState<MembroModel[]>([]);
    const [eventoSemanal, setEventoSemanal] = useState<EventoSemanalModel[]>([]);
    const [carregamento, setCarregando] = useState<boolean>(false);

    async function consultaStorage() {
        const evenSem: EventoSemanalModel[] = []
        const membrosFirebase: MembroModel[] = [];
        await firebase.database().ref(`tb_membro/${celulaObj?.id}`).on('value', response => {
            response.forEach(resp => {
                membrosFirebase.push({ id: resp.key, ...resp.val() })
            });
            setMembros(membrosFirebase)
        })
        await firebase.database().ref('tb_evento/semanal').on('value', result => {
            result.forEach(evtSem => {
                evenSem.push({ id: evtSem.key, ...evtSem.val() })
            })
        });
        setEventoSemanal(evenSem)
    }

    async function loadItem(data: Date) {
        let objeto: Object = {};
        let mark: Object = {}
        await setTimeout(() => {
            for (let i = -75; i < 40; i++) {
                const time = new Date().setDate(data.getDate() + i)
                const strTime = timeToString(time);
                const weekDay: number = getWeekDay(time);
                objeto[strTime] = [];
                if (weekDay == celulaObj?.dia && i <= 14) {
                    if (listaDataRel.includes(strTime)) {
                        objeto[strTime].push({ nome: celulaObj?.nome, celula: true, horario: celulaObj?.horario, relatorio: true, dia: strTime })
                        mark[strTime] = { marked: true, dotColor: 'green', selectedColor: 'green' };
                    } else {
                        objeto[strTime].push({ nome: celulaObj?.nome, celula: true, horario: celulaObj?.horario, relatorio: false, dia: strTime })
                        mark[strTime] = { marked: true, dotColor: 'red', selectedColor: 'red' };
                    }
                }
                eventoSemanal.forEach(sem => {
                    if (weekDay == sem.diaSemana && i <= 14) {
                        objeto[strTime].push({ celula: false, eventoSemanal: sem })
                        mark[strTime] = { marked: true, dotColor: '#d3d3d3', selectedColor: '#d3d3d3' }
                    }
                })
            }
            setItems(objeto);
            setMarked(mark);
            setCarregando(true)
        }, 1000)
    }

    function timeToString(time: number): string {
        const date: Date = new Date(time);
        const dateIso = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
        const data = new Date(dateIso)
        return data.toISOString().split('T')[0];
    }
    function getWeekDay(time: number): number {
        const date: Date = new Date(time);
        return date.getDay();
    }

    function minEMaxData(dias: number): string {
        const data = new Date().setDate(new Date().getDate() + dias);
        return new Date(data).toISOString().split('T')[0];
    }

    useEffect(() => {
        consultaStorage();
    }, []);

    useEffect(() => {
        loadItem(new Date());
        consultaStorage()
    }, [celulaObj, listaDataRel]);

    if (!carregamento) {
        return <View style={styles.carregamento}>
            <ActivityIndicator size="large" color="black" />
        </View>
    }

    return (
        <>
        
            <Agenda
                // style={styles}
                pastScrollRange={qtdMeses}
                futureScrollRange={2}
                items={items}
                minDate={minEMaxData(-75)}
                // minDate={diaInicio}
                maxDate={minEMaxData(29)}
                selected={new Date().toISOString().split('T')[0]}
                // markingType={'custom'}
                markedDates={marked as any}
                // refreshing={true}
                renderItem={(item: EventoProps, firstDayInDay: boolean) => {
                    return (
                        item.celula ?
                            <EventoCelula
                                evento={item}
                                celula={celulaObj}
                                membros={membros}
                            />
                            : <Evento eventoSemanal={item.eventoSemanal} />
                    )
                }}
                theme={{
                    // agendaDayNumColor: 'black',
                    // agendaKnobColor: '#fff',
                }}
            />
        </>
    )
};

export default Calendario;