import React from 'react';
import { View, Text, Image } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

import logoBrancoAlvo from '../../assets/images/logo-alvo-branco.png';

import styles from './style';

interface PageHeaderProps {
    title: any,
}

const PageHeader: React.FC<PageHeaderProps> = ({ title }) => {
    return (
        <View style={styles.container}>
            <View style={styles.topBar}>
                <AntDesign name="arrowleft" size={20} color="#FFF"/>
                <Text style={styles.title}>{title}</Text>
                <Image source={logoBrancoAlvo} style={styles.imageLogo}/>
            </View>
        </View>
    )
};

export default PageHeader;