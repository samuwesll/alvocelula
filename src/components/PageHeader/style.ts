import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        padding: 10,
        backgroundColor: '#5a5a60',
        width: '100%'
    },
    topBar: {
        padding: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        color: '#FFF',
        fontSize: 20,
        fontFamily: 'Caladea_400Regular_Italic',
    },
    imageLogo: {
        width: 40,
        height: 40,
    },
});

export default styles;