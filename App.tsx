import React, { useEffect } from 'react';
import { StyleSheet, StatusBar } from 'react-native';
import { AppLoading } from 'expo';
import { useFonts, Roboto_400Regular, Roboto_500Medium } from '@expo-google-fonts/roboto';
import { Caladea_400Regular_Italic } from '@expo-google-fonts/caladea';
import AppStack from './src/routes/AppStack';

export default function App() {

  const [fontsLoaded] = useFonts({
    Roboto_400Regular,
    Roboto_500Medium,
    Caladea_400Regular_Italic,
  })
  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <>
      <AppStack />
      <StatusBar 
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent
      />
    </>
  );
}

const styles = StyleSheet.create({

});
